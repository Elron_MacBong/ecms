<?php
if(!defined('eCMS')) die('Hacking attempt...');

// NEEDED MODULE-VARIALES
$m_title		= 'Ausloggen';

// VARIABLES
$error			= false;
$errorString	= '';

if($this->auth->checkPermission('logout') === true) {
	$this->security->logout();
	header('LOCATION: '.GENERAL_PAGE_URI.'login/');
	
	$m_title = $this->setPageTitle($m_title);
} else {
	$m_title = $this->setPageTitle('Error');
	$errorString = 'Nicht genug Rechte oder nicht angemeldet!';
	
	$tpl = '_error';
}

$this->assign('pageTitle',		$m_title);
$this->assign('error',			$error);
$this->assign('errorString',	$errorString);

$this->display($tpl);
?>