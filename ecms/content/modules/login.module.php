<?php
if(!defined('eCMS')) die('Hacking attempt...');

// NEEDED MODULE-VARIALES
$m_title		= 'Einloggen';

// VARIABLES
$error			= false;
$errorString	= '';
$successString	= '';

$email			= '';
$password		= '';
$autoLogin		= false;

$emailVal		= '';
$passwordVal	= '';

if($this->auth->checkPermission('login') === true) {
	if(isset($_SESSION['eCMSuserUID'])) {
		$row = $this->db->getArray("SELECT email FROM {$this->db_prefix}user WHERE uid = '".$this->db->secureString($_SESSION['eCMSuserUID'])."'");
		if(count($row) === 1) $emailVal = $row[0]['email'];
	}
	
	// Now run the login-things
	if(isset($_POST['submitLogin'])) {
		// First, read out the salt and save all the variables to easier handling.
		$email			= $this->db->secureString($_POST['email-field']);
		$password		= $this->db->secureString($_POST['password-field']);
		
		$emailVal		= $email;
		$passwordVal	= $password;
		
		// Check for autologin.
		if(isset($_POST['autoLogin'])) $autoLogin = true;
		
		
		
		// We call the login-function and save the return.
		$loginReturn = $this->security->login($email, $password, $autoLogin);
		// Now we run login-checks.
		if($loginReturn === 'disabled') $errorString = 'Dieser Account ist leider deaktiviert!';
		if($loginReturn === 'activated') $errorString = 'Dieser Account wurde noch nicht aktiviert!<br />Bitte E-Mails &uuml;berpr&uuml;fen.';
		if($loginReturn === 'int_error') $errorString = 'Interner Fehler entdeckt!<br />Webmaster wurde informiert. Bitte versuche es sp&auml;ter erneut.';
		if($loginReturn === false) $errorString = 'E-Mail oder Passwort stimmen nicht oder dieser Account existiert nicht!';
		if($loginReturn === true) {
			$successString = '<b>Willkommen</b><br />Du wurdest erfolgreich eingeloggt.<br />Du wirst in 3 Sekunden weitergeleitet.<br /><br /><a href="'.GENERAL_PAGE_URI.'profile/">Hier kommst du zu deinem Profil.</a><br />';
			
			header('REFRESH:3; URL='.GENERAL_PAGE_URI.'profile/');
		}
	}
	
	$m_title = $this->setPageTitle($m_title);
	
	$tpl = 'login';
} else {
	$m_title = $this->setPageTitle('Error');
	$errorString = 'Du bist bereits eingeloggt!';
	
	$tpl = '_error';
}

$this->assign('pageTitle',		$m_title);
$this->assign('error',			$error);
$this->assign('errorString',	$errorString);
$this->assign('successString',	$successString);

$this->assign('email',			$emailVal);
$this->assign('password',		$passwordVal);
$this->assign('autoLogin',		$autoLogin);

$this->display($tpl);
?>