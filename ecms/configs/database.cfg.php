<?php
if(!defined('eCMS')) die('Hacking attempt...');

if(!defined('DB_DEFAULT_01')) define('DB_DEFAULT_01', 1);

/*********************\
|* Database settings *|
\*********************/
$MYSQL_SETTINGS[1]['host']    = '127.0.0.1';
$MYSQL_SETTINGS[1]['port']    = 3306;
$MYSQL_SETTINGS[1]['user']    = 'root';
$MYSQL_SETTINGS[1]['pass']    = '';
$MYSQL_SETTINGS[1]['db']      = 'ecms';
$MYSQL_SETTINGS[1]['prefix']  = 'ecms_';
$MYSQL_SETTINGS[1]['charSet'] = 'utf8';
?>