<?php
/*******************************************************\
| Functions:											|
| - void	public	__construct(object eCMS);			|
| - void	public	__destruct();						|
\*******************************************************/

if(!defined('eCMS')) die('Hacking attempt...');

class Protection {
	private $eCMS;
	
	public function __construct($eCMS) {
		$this->eCMS = $eCMS;
	}
	
	
	
	public function __destruct() {}
}
?>