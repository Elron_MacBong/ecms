<?php
/*******************************************************\
| Functions:											|
| - void	public	__construct(object eCMS);			|
| - array	public	getUser(string $uid = NULL);		|
| - array	public	getUsers();							|
| - bool	public	checkFriend(string $uid);			|
| - string	public	getFriendState(string $uid);		|
| - bool	public	checkAdd(string $uid);				|
| - bool	public	addToFl(string $uid);				|
| - void	public	acceptAdd(string $uid,				|
|							 bool $accept = true);		|
| - bool	public	checkUID(string $uid);				|
| - bool	public	checkUsername(string $username);	|
| - bool	public	checkEmail(string $email);			|
| - bool	public	checkSalt(string $salt);			|
| - void	public	__destruct();						|
\*******************************************************/

require_once(DIR_LIB_ECMS.'classes/profile.class.php');

if(!defined('eCMS')) die('Hacking attempt...');

class User extends Profile {
	private $eCMS;
	
	private $uid;
	private $ual;
	private $username;
	private $password;
	private $salt;
	private $email;
	private $registered;
	private $activated;
	private $activation_key;
	private $disabled;
	private $language;
	private $last_login;
	private $last_action;
	private $online;
	private $auto_login;
	private $session_id;
	private $ip;
	private $port;
	private $hostname;
	private $user_agent;
	private $secret_question;
	private $secret_answer;
	private $chat_room_id;
	private $chat_last_action;
	private $chat_last_msg;
	private $seo;
	
	// 
	# @param object	$eCMS
	# 
	# @return void
	public function __construct($eCMS) {
		parent::__construct($eCMS);
		
		$this->eCMS = $eCMS;
		
		if($this->eCMS->security->checkLogin() === true) {
			$this->uid = $_SESSION['eCMSuserUID'];
			
			$row = $this->eCMS->db->getArray("SELECT 
													ual, 
													username, 
													password, 
													salt, 
													email, 
													registered, 
													activated, 
													activation_key, 
													disabled, 
													language, 
													last_login, 
													last_action, 
													online, 
													auto_login, 
													session_id, 
													ip, 
													port, 
													hostname, 
													user_agent, 
													secret_question, 
													secret_answer, 
													chat_room_id, 
													chat_last_action, 
													chat_last_msg, 
													seo 
												FROM 
													{$this->eCMS->db_prefix}user 
												WHERE 
													uid = '".$this->eCMS->db->secureString($this->uid)."'");
			
			$row = $row[0];
			
			$this->ual					= $row['ual'];
			$this->username				= $row['username'];
			$this->password				= $row['password'];
			$this->salt					= $row['salt'];
			$this->email				= $row['email'];
			$this->registered			= $row['registered'];
			$this->activated			= $row['activated'];
			$this->activation_key		= $row['activation_key'];
			$this->disabled				= $row['disabled'];
			$this->language				= $row['language'];
			$this->lastLogin			= $row['last_login'];
			$this->lastAction			= $row['last_action'];
			$this->online				= $row['online'];
			$this->auto_login			= $row['auto_login'];
			$this->session_id			= $row['session_id'];
			$this->ip					= $row['ip'];
			$this->port					= $row['port'];
			$this->hostname				= $row['hostname'];
			$this->user_agent			= $row['user_agent'];
			$this->secret_question		= $row['secret_question'];
			$this->secret_answer		= $row['secret_answer'];
			$this->chat_room_id			= $row['chat_room_id'];
			$this->chat_last_action		= $row['chat_last_action'];
			$this->chat_last_msg		= $row['chat_last_msg'];
			$this->seo					= $row['seo'];
		}
	}
	
	
	
	// 
	# @param int	$uid	(default: NULL)
	# 
	# @return array
	public function getUser($uid = NULL) {
		if($uid === NULL) {
			$uid				= $this->uid;
			$ual				= $this->ual;
			$username			= $this->username;
			$password			= $this->password;
			$salt				= $this->salt;
			$email				= $this->email;
			$registered			= $this->registered;
			$activated			= $this->activated;
			$activation_key		= $this->activation_key;
			$disabled			= $this->disabled;
			$language			= $this->language;
			$last_login			= $this->last_login;
			$last_action		= $this->last_action;
			$online				= $this->online;
			$auto_login			= $this->auto_login;
			$session_id			= $this->session_id;
			$ip					= $this->ip;
			$port				= $this->port;
			$hostname			= $this->hostname;
			$user_agent			= $this->user_agent;
			$secret_question	= $this->secret_question;
			$secret_answer		= $this->secret_answer;
			$chat_room_id		= $this->chat_room_id;
			$chat_last_action	= $this->chat_last_action;
			$chat_last_msg		= $this->chat_last_msg;
			$seo				= $this->seo;
		} else {
			if(!ctype_digit($uid)) {
				$row = $this->eCMS->db->getArray("SELECT uid FROM {$this->eCMS->db_prefix}user WHERE seo = '".$this->eCMS->db->secureString($uid)."'");
				
				if(count($row) === 1) $uid = $row[0]['uid'];
			}
			
			$row = $this->eCMS->db->getArray("SELECT 
													uid, 
													ual, 
													username, 
													password, 
													salt, 
													email, 
													registered, 
													activated, 
													activation_key, 
													disabled, 
													language, 
													last_login, 
													last_action, 
													online, 
													auto_login, 
													session_id, 
													ip, 
													port, 
													hostname, 
													user_agent, 
													secret_question, 
													secret_answer, 
													chat_room_id, 
													chat_last_action, 
													chat_last_msg, 
													seo 
												FROM 
													{$this->eCMS->db_prefix}user 
												WHERE 
													uid = '".$this->eCMS->db->secureString($uid)."'");
			
			if(count($row) === 1) {
				$uid				= $row[0]['uid'];
				$ual				= $row[0]['ual'];
				$username			= $row[0]['username'];
				$password			= $row[0]['password'];
				$salt				= $row[0]['salt'];
				$email				= $row[0]['email'];
				$registered			= $row[0]['registered'];
				$activated			= $row[0]['activated'];
				$activation_key		= $row[0]['activation_key'];
				$disabled			= $row[0]['disabled'];
				$language			= $row[0]['language'];
				$last_login			= $row[0]['last_login'];
				$last_action		= $row[0]['last_action'];
				$online				= $row[0]['online'];
				$auto_login			= $row[0]['auto_login'];
				$session_id			= $row[0]['session_id'];
				$ip					= $row[0]['ip'];
				$port				= $row[0]['port'];
				$hostname			= $row[0]['hostname'];
				$user_agent			= $row[0]['user_agent'];
				$secret_question	= $row[0]['secret_question'];
				$secret_question	= $row[0]['secret_answer'];
				$chat_room_id		= $row[0]['chat_room_id'];
				$chat_last_action	= $row[0]['chat_last_action'];
				$chat_last_msg		= $row[0]['chat_last_msg'];
				$seo				= $row[0]['seo'];
			} else return false;
		}
		
		$arr = array(	'uid'					=> $uid,
						'ual'					=> $ual,
						'username'				=> $username,
						'password'				=> $password,
						'salt'					=> $salt,
						'email'					=> $email,
						'registered'			=> $registered,
						'activated'				=> $activated,
						'activation_key'		=> $activation_key,
						'disabled'				=> $disabled,
						'language'				=> $language,
						'last_login'			=> $last_login,
						'last_action'			=> $last_action,
						'online'				=> $online,
						'auto_login'			=> $auto_login,
						'session_id'			=> $session_id,
						'ip'					=> $ip,
						'port'					=> $port,
						'hostname'				=> $hostname,
						'user_agent'			=> $user_agent,
						'secret_question'		=> $secret_question,
						'secret_answer'			=> $secret_question,
						'chat_room_id'			=> $chat_room_id,
						'chat_last_action'		=> $chat_last_action,
						'chat_last_msg'			=> $chat_last_msg,
						'seo'					=> $seo);
		
		return $arr;
	}
	
	// 
	# @return array
	public function getUsers() {
		$arr = $this->eCMS->db->getArray("SELECT 
												uid, 
												ual, 
												username, 
												password, 
												salt, 
												email, 
												registered, 
												activated, 
												activation_key, 
												disabled, 
												language, 
												last_login, 
												last_action, 
												online, 
												auto_login, 
												session_id, 
												ip, 
												port, 
												hostname, 
												user_agent, 
												secret_question, 
												secret_answer, 
												chat_room_id, 
												chat_last_action, 
												chat_last_msg, 
												seo 
											FROM 
												{$this->eCMS->db_prefix}user 
											ORDER BY 
												username 
											ASC");
		
		return $arr;
	}
	
	// 
	# @param string	$uid
	# 
	# @return boolean
	public function checkFriend($uid) {
		if(!is_string($uid)) $this->eCMS->dieFunctionCall('checkFriend', 'uid', gettype($uid), 'string');
		
		if($this->eCMS->security->checkLogin() === true) {
			$row = $this->eCMS->db->getArray("SELECT 
													fUID2 AS fUID 
												FROM 
													{$this->eCMS->db_prefix}user_friendlist 
												WHERE 
													fUID1 = '".$this->eCMS->db->secureString($_SESSION['eCMSuserUID'])."' 
												UNION SELECT 
													fUID1 AS fUID 
												FROM 
													{$this->eCMS->db_prefix}user_friendlist 
												WHERE 
													fUID2 = '".$this->eCMS->db->secureString($_SESSION['eCMSuserUID'])."'");
			
			for($i = 0; $i < count($row); $i++) {
				if($row[$i]['fUID'] === $uid && $row[$i]) return true;
			}
			
			return false;
		} else return false;
	}
	// 
	# @param string	$uid
	# 
	# @return int
	public function getFriendState($uid) {
		if(!is_string($uid)) $this->eCMS->dieFunctionCall('getFriendState', 'uid', gettype($uid), 'string');
		
		if($this->eCMS->security->checkLogin() === true) {
			$row = $this->eCMS->db->getArray("SELECT 
													fUID2 AS fUID, 
													status AS status 
												FROM 
													{$this->eCMS->db_prefix}user_friendlist 
												WHERE 
													fUID1 = '".$this->eCMS->db->secureString($_SESSION['eCMSuserUID'])."' 
												UNION SELECT 
													fUID1 AS fUID, 
													status AS status 
												FROM 
													{$this->eCMS->db_prefix}user_friendlist 
												WHERE 
													fUID2 = '".$this->eCMS->db->secureString($_SESSION['eCMSuserUID'])."'");
			
			for($i = 0; $i < count($row); $i++) {
				if($row[$i]['fUID'] === $uid && $row[$i]) return $row[$i]['status'];
			}
			
			return false;
		} else return false;
	}
	
	// 
	# @param string	$uid
	# 
	# @return boolean
	public function checkAdd($uid) {
		if(!is_string($uid)) $this->eCMS->dieFunctionCall('getFriendState', 'uid', gettype($uid), 'string');
		
		if($this->eCMS->security->checkLogin() === true) {
			$row = $this->eCMS->db->getArray("SELECT 
													fUID1 AS fUID 
												FROM 
													{$this->eCMS->db_prefix}user_friendlist 
												WHERE 
													fUID2 = '".$this->eCMS->db->secureString($_SESSION['eCMSuserUID'])."' 
												AND status = '0'");
			
			if(count($row) === 1) {
				if($row[0]['fUID'] === $uid) return true;
				else return false;
			} else return false;
		} else return false;
	}
	
	// 
	# @param string	$uid
	# 
	# @return boolean
	public function addToFl($uid) {}
	
	// 
	# @param string	$uid
	# @param bool	$accept	(default: true)
	# 
	# @return void
	public function acceptAdd($uid, $accept = true) {}
	
	// 
	# @param string	$uid
	# 
	# @return boolean
	public function checkUID($uid) {}
	
	// 
	# @param string	$username
	# 
	# @return boolean
	public function checkUsername($username) {}
	
	// 
	# @param string	$email
	# 
	# @return boolean
	public function checkEmail($email) {}
	
	// 
	# @param string	$salt
	# 
	# @return boolean
	public function checkSalt($salt) {}
	
	
	
	// 
	# @return void
	public function __destruct() {}
}
?>