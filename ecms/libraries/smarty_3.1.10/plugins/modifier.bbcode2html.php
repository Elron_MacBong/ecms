<?php
/*
 * Smarty plugin
 * ------------------------------------------------------------
 * Type:       modifier
 * Name:       bbcode2html
 * Purpose:    Converts BBCode style tags to HTML
 * Author:     Andre Rabold
 * Version:    1.4
 * Remarks:    Notice that this function does not check for
 *             correct syntax. Try not to use it with invalid
 *             BBCode because this could lead to unexpected
 *             results ;-)
 *             It seems that this function ignores manual 
 *             line breaks. IMO this can be fixed by adding 
 *             '/\n/' => "<br>" to $preg
 *
 * What's new: - Rewrote some preg expressions for more
 *               stability.
 *             - renamed CSS classes to be more generic. (Example
 *               CSS file attached.)
 *             - Support for escaped tags. Add a backslash
 *               infront of a tag if you don't want to transform
 *               it. For example: \[b]
 *
 *             Version 1.3c
 *             - Fixed a bug with <li>...</li> tags (thanks
 *               to Rob Schultz for pointing this out)
 *
 *             Version 1.3b
 *             - Added more support for phpBB2:
 *               [list]...[/list:u] unordered lists
 *               [list]...[/list:o] ordered lists
 *             
 *             Version 1.3
 *             - added support for phpBB2 like tag identifier
 *               like [b:b6a0cef7ea]This is bold[/b:b6a0cef7ea]
 *               (thanks to Rob Schultz)
 *             - added support for quotes within the quote tag
 *               so [quote="foo"]bar[/quote] does work now
 *               correctly
 *             - removed str_replace functions
 *
 *             Version 1.2
 *             - now supports CSS classes:
 *                  ng_email      (mailto links)
 *                  ng_url        (www links)
 *                  ng_quote      (quotes)
 *                  ng_quote_body (quotes)
 *                  ng_code       (source code)
 *                  ng_list       (html lists)
 *                  ng_list_item  (list items)
 *             - replaced slow ereg_replace() functions
 *             - Alterned [quote] and [code] to use CSS classes
 *               instead of HTML <blockquote />, <hr />, ... tags.
 *             - Additional BBCode tags [list] and [*] to display
 *               nice HTML lists. Example:
 *                 [list]
 *                   [*]first item
 *                   [*]second item
 *                   [*]third item
 *                 [/list]
 *               The [list] tag can have an additional parameter:
 *                 [list]   unorderer list with bullets
 *                 [list=1] ordered list 1,2,3,4,...
 *                 [list=i] ordered list i,ii,iii,iv,...
 *                 [list=I] ordered list I,II,III,IV,...
 *                 [list=a] ordered list a,b,c,d,...
 *                 [list=A] ordered list A,B,C,D,...
 *             - produces well-formed output
 *             - cleaned up the code
 * ------------------------------------------------------------
 */
function smarty_modifier_bbcode2html($message) {
  $preg = array(
		  // [color=#COLOR-CODE]TEXT[/color]
          '/(?<!\\\\)\[color(?::\w+)?=(.*?)\](.*?)\[\/color(?::\w+)?\]/si'   => "<span style=\"color:\\1\">\\2</span>",
		  // [size=SIZE]TEXT[/size]
          '/(?<!\\\\)\[size(?::\w+)?=(.*?)\](.*?)\[\/size(?::\w+)?\]/si'     => "<span style=\"font-size:\\1\">\\2</span>",
		  // [font=FAMILY]TEXT[/font]
          '/(?<!\\\\)\[font(?::\w+)?=(.*?)\](.*?)\[\/font(?::\w+)?\]/si'     => "<span style=\"font-family:\\1\">\\2</span>",
		  // [align=LEFT|CENTER|RIGHT]TEXT[/align]
          '/(?<!\\\\)\[align(?::\w+)?=(.*?)\](.*?)\[\/align(?::\w+)?\]/si'   => "<div style=\"text-align:\\1\">\\2</div>",
		  // [b]TEXT[/b]
          '/(?<!\\\\)\[b(?::\w+)?\](.*?)\[\/b(?::\w+)?\]/si'                 => "<span style=\"font-weight:bold\">\\1</span>",
		  // [i]TEXT[/i]
          '/(?<!\\\\)\[i(?::\w+)?\](.*?)\[\/i(?::\w+)?\]/si'                 => "<span style=\"font-style:italic\">\\1</span>",
		  // [u]TEXT[/u]
          '/(?<!\\\\)\[u(?::\w+)?\](.*?)\[\/u(?::\w+)?\]/si'                 => "<span style=\"text-decoration:underline\">\\1</span>",
		  // [center]TEXT[/center]
          '/(?<!\\\\)\[center(?::\w+)?\](.*?)\[\/center(?::\w+)?\]/si'       => "<div style=\"text-align:center\">\\1</div>",

          // [code]CODE-TEXT[/code]
          '/(?<!\\\\)\[code(?::\w+)?\](.*?)\[\/code(?::\w+)?\]/si'           => "<div class=\"bb-code\">\\1</div>",
		  // [php]PHP-TEXT[/php]
          '/(?<!\\\\)\[php(?::\w+)?\](.*?)\[\/php(?::\w+)?\]/si'             => "<div class=\"bb-php\">\\1</div>",
          // [email]EMAIL[/email]
          '/(?<!\\\\)\[email(?::\w+)?\](.*?)\[\/email(?::\w+)?\]/si'         => "<a href=\"mailto:\\1\" class=\"bb-email\">\\1</a>",
          '/(?<!\\\\)\[email(?::\w+)?=(.*?)\](.*?)\[\/email(?::\w+)?\]/si'   => "<a href=\"mailto:\\1\" class=\"bb-email\">\\2</a>",
          // [url]example.com[/url]
          '/(?<!\\\\)\[url(?::\w+)?\]www\.(.*?)\[\/url(?::\w+)?\]/si'        => "<a href=\"http://www.\\1\" target=\"_blank\" class=\"bb-url\">\\1</a>",
		  // [url]www.examle.com[/url]
          '/(?<!\\\\)\[url(?::\w+)?\](.*?)\[\/url(?::\w+)?\]/si'             => "<a href=\"\\1\" target=\"_blank\" class=\"bb-url\">\\1</a>",
		  // [url=www.example.com]LINK-TEXT[/url]
          '/(?<!\\\\)\[url(?::\w+)?=(.*?)?\](.*?)\[\/url(?::\w+)?\]/si'      => "<a href=\"\\1\" target=\"_blank\" class=\"bb-url\">\\2</a>",
          // [img]IMAGE-LINK[/img]
          '/(?<!\\\\)\[img(?::\w+)?\](.*?)\[\/img(?::\w+)?\]/si'             => "<img src=\"\\1\" alt=\"\\1\" class=\"bb-image\" />",
		  // [img=WIDTHxHEIGHT]IMAGE-LINK[/img]
          '/(?<!\\\\)\[img(?::\w+)?=(.*?)x(.*?)\](.*?)\[\/img(?::\w+)?\]/si' => "<img width=\"\\1\" height=\"\\2\" src=\"\\3\" alt=\"\\3\" class=\"bb-image\" />",
          // [quote]TEXT[/quote]
          '/(?<!\\\\)\[quote(?::\w+)?\](.*?)\[\/quote(?::\w+)?\]/si'         => "<div>Zitat:<br /><div class=\"bb-quote\">\\1</div></div>",
		  // [quote=AUTHOR]TEXT[/quote]
          '/(?<!\\\\)\[quote(?::\w+)?=(?:&quot;|"|\')?(.*?)["\']?(?:&quot;|"|\')?\](.*?)\[\/quote\]/si'   => "<div>Zitat von \\1:<br /><div class=\"bb-quote\">\\2</div></div>",
          // [list]LIST[/list]
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[\*(?::\w+)?\](.*?)(?=(?:\s*<br\s*\/?>\s*)?\[\*|(?:\s*<br\s*\/?>\s*)?\[\/?list)/si' => "\n<li class=\"bb-listitem\">\\1</li>",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[\/list(:(?!u|o)\w+)?\](?:<br\s*\/?>)?/si'    => "\n</ul>",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[\/list:u(:\w+)?\](?:<br\s*\/?>)?/si'         => "\n</ul>",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[\/list:o(:\w+)?\](?:<br\s*\/?>)?/si'         => "\n</ol>",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list(:(?!u|o)\w+)?\]\s*(?:<br\s*\/?>)?/si'   => "\n<ul class=\"bb-list-unordered\">",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list:u(:\w+)?\]\s*(?:<br\s*\/?>)?/si'        => "\n<ul class=\"bb-list-unordered\">",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list:o(:\w+)?\]\s*(?:<br\s*\/?>)?/si'        => "\n<ol class=\"bb-list-ordered\">",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list(?::o)?(:\w+)?=1\]\s*(?:<br\s*\/?>)?/si' => "\n<ol class=\"bb-list-ordered,bb-list-ordered-d\">",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list(?::o)?(:\w+)?=i\]\s*(?:<br\s*\/?>)?/s'  => "\n<ol class=\"bb-list-ordered,bb-list-ordered-lr\">",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list(?::o)?(:\w+)?=I\]\s*(?:<br\s*\/?>)?/s'  => "\n<ol class=\"bb-list-ordered,bb-list-ordered-ur\">",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list(?::o)?(:\w+)?=a\]\s*(?:<br\s*\/?>)?/s'  => "\n<ol class=\"bb-list-ordered,bb-list-ordered-la\">",
          '/(?<!\\\\)(?:\s*<br\s*\/?>\s*)?\[list(?::o)?(:\w+)?=A\]\s*(?:<br\s*\/?>)?/s'  => "\n<ol class=\"bb-list-ordered,bb-list-ordered-ua\">",
          // escaped tags like \[b], \[color], \[url], ...
          '/\\\\(\[\/?\w+(?::\w+)*\])/'                                      => "\\1",
		  
		  // Smilies
		  // :) :-)
		  '/\:\)/si'			=> '<img src="'.IMAGES_PATH.'smilies/smiley.gif" border="0" />',
		  '/\:\-\)/si'			=> '<img src="'.IMAGES_PATH.'smilies/smiley.gif" border="0" />',
		  // ;) ;-)
		  '/\;\)/si'			=> '<img src="'.IMAGES_PATH.'smilies/wink.gif" border="0" />',
		  '/\;\-\)/si'			=> '<img src="'.IMAGES_PATH.'smilies/wink.gif" border="0" />',
		  // :( :-(
		  '/\:\(/si'			=> '<img src="'.IMAGES_PATH.'smilies/sad.gif" border="0" />',
		  '/\:\-\(/si'			=> '<img src="'.IMAGES_PATH.'smilies/sad.gif" border="0" />',
		  // :D :-D
		  '/\:D/si'				=> '<img src="'.IMAGES_PATH.'smilies/icon_mrgreen.gif" border="0" />',
		  '/\:\-D/si'			=> '<img src="'.IMAGES_PATH.'smilies/icon_mrgreen.gif" border="0" />',
		  // :O :-O
		  '/\:O/si'				=> '<img src="'.IMAGES_PATH.'smilies/smileO.gif" border="0" />',
		  '/\:\-O/si'			=> '<img src="'.IMAGES_PATH.'smilies/smileO.gif" border="0" />',
		  // :X :-X :* :-* :kiss:
		  '/\:X/si'				=> '<img src="'.IMAGES_PATH.'smilies/kiss.gif" border="0" />',
		  '/\:\-X/si'			=> '<img src="'.IMAGES_PATH.'smilies/kiss.gif" border="0" />',
		  '/\:\*/si'			=> '<img src="'.IMAGES_PATH.'smilies/kiss.gif" border="0" />',
		  '/\:\-\*/si'			=> '<img src="'.IMAGES_PATH.'smilies/kiss.gif" border="0" />',
		  '/\:kiss:/si'			=> '<img src="'.IMAGES_PATH.'smilies/kiss.gif" border="0" />',
		  // :P :-P :tong:
		  '/\:P/si'				=> '<img src="'.IMAGES_PATH.'smilies/tong.gif" border="0" />',
		  '/\:\-P/si'			=> '<img src="'.IMAGES_PATH.'smilies/tong.gif" border="0" />',
		  '/\:tong:/si'			=> '<img src="'.IMAGES_PATH.'smilies/tong.gif" border="0" />',
		  // ;P ;-P :frech:
		  '/\;P/si'				=> '<img src="'.IMAGES_PATH.'smilies/frech.gif" border="0" />',
		  '/\;\-P/si'			=> '<img src="'.IMAGES_PATH.'smilies/frech.gif" border="0" />',
		  '/\:frech:/si'		=> '<img src="'.IMAGES_PATH.'smilies/frech.gif" border="0" />',
		  // 8) 8-) :cool:
		  '/8\)/si'				=> '<img src="'.IMAGES_PATH.'smilies/cool.gif" border="0" />',
		  '/8\-\)/si'			=> '<img src="'.IMAGES_PATH.'smilies/cool.gif" border="0" />',
		  '/:cool:/si'			=> '<img src="'.IMAGES_PATH.'smilies/cool.gif" border="0" />',
		  
		  // 8) 8-) :cool:
		  '/8\)/si'				=> '<img src="'.IMAGES_PATH.'smilies/cool.gif" border="0" />',
		  
		  
		  
		  // :-)
		  //'/\:\-\)/si'			=> '<img src="'.imagesPath.'smilies/smiley.gif" border="0" />',
		  // ;-)
		  //'/\;\-\)/si'			=> '<img src="'.imagesPath.'smilies/wink.gif" border="0" />',
		  // :-*
		  //'/\:\-\*/si'			=> '<img src="'.imagesPath.'smilies/kiss.gif" border="0" />',
		  
		  // :)
		  //'/\:\)/si'			=> '<img src="'.imagesPath.'smilies/smiley.gif" border="0" />',
		  // ;)
		  //'/\;\)/si'			=> '<img src="'.imagesPath.'smilies/wink.gif" border="0" />',
		  // :*
		  //'/\:\*/si'			=> '<img src="'.imagesPath.'smilies/kiss.gif" border="0" />',
		  
		  // xD
		  //'/xD/si'				=> '<img src="'.imagesPath.'smilies/laugh.gif" border="0" />',
		  
		  /*'/\:afro:/si'			=> '<img src="'.imagesPath.'smilies/afro.gif" border="0" />',
		  '/\:angel:/si'		=> '<img src="'.imagesPath.'smilies/angel.gif" border="0" />',
		  '/\:angry:/si'		=> '<img src="'.imagesPath.'smilies/angry.gif" border="0" />',
		  '/\:blank:/si'		=> '<img src="'.imagesPath.'smilies/blank.gif" border="0" />',
		  '/\:cheesy:/si'		=> '<img src="'.imagesPath.'smilies/cheesy.gif" border="0" />',
		  '/\:cool:/si'			=> '<img src="'.imagesPath.'smilies/cool.gif" border="0" />',
		  '/\:cry:/si'			=> '<img src="'.imagesPath.'smilies/cry.gif" border="0" />',
		  '/\:embarrassed:/si'	=> '<img src="'.imagesPath.'smilies/embarrassed.gif" border="0" />',
		  '/\:evil:/si'			=> '<img src="'.imagesPath.'smilies/evil.gif" border="0" />',
		  '/\:grin:/si'			=> '<img src="'.imagesPath.'smilies/grin.gif" border="0" />',
		  '/\:huh:/si'			=> '<img src="'.imagesPath.'smilies/huh.gif" border="0" />',
		  '/\:lipsrsealed:/si'	=> '<img src="'.imagesPath.'smilies/lipsrsealed.gif" border="0" />',
		  '/\:police:/si'		=> '<img src="'.imagesPath.'smilies/police.gif" border="0" />',
		  '/\:rolleyes:/si'		=> '<img src="'.imagesPath.'smilies/rolleyes.gif" border="0" />',
		  '/\:sad:/si'			=> '<img src="'.imagesPath.'smilies/sad.gif" border="0" />',
		  '/\:shocked:/si'		=> '<img src="'.imagesPath.'smilies/shocked.gif" border="0" />',
		  '/\:tongue:/si'		=> '<img src="'.imagesPath.'smilies/tongue.gif" border="0" />',
		  '/\:undecided:/si'	=> '<img src="'.imagesPath.'smilies/undecided.gif" border="0" />',*/
  );
  $message = preg_replace(array_keys($preg), array_values($preg), $message);
  return $message;
}
?>