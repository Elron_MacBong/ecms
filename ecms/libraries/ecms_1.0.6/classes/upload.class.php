<?php
/*******************************************************\
| Functions:											|
| - void	public	__construct(object eCMS);			|

| - void	public	__destruct();						|
\*******************************************************/

if(!defined('eCMS')) die('Hacking attempt...');

class Upload {
	private $eCMS;
	
	private $name;
	private $type;
	private $size;
	private $media_type;
	private $tmp_name;
	private $error;
	
	private $uploadDir;
	private $uploadFile;
	private $errors;
	
	public function __construct($eCMS) {
		$this->eCMS = $eCMS;
	}
	
	
	
	// 
	public function upload($name, $type, $size, $media_type, $tmp_name, $error, $tmp = false) {
		$this->name			= basename($name);
		$this->type			= $type;
		$this->size			= $size;
		$this->media_type	= $media_type;
		$this->tmp_name		= $tmp_name;
		$this->error		= $error;
		$this->tmp			= $tmp;
		
		if($this->error == 0) {
			switch($this->type) {
				// Application
				case 'application/gzip':
					break;
				
				case 'application/pdf':
					break;
				
				case 'application/rtf':
					break;
				
				case 'application/xhtml+xml':
					break;
				
				case 'application/xml':
					break;
				
				case 'application/x-compress':
					break;
				
				case 'application/x-gtar':
					break;
				
				case 'application/x-httpd-php':
					break;
				
				case 'application/x-javascript':
					break;
				
				case 'application/x-sh':
					break;
				
				case 'application/x-shockwave-flash':
					break;
				
				case 'application/x-tar':
					break;
				
				case 'application/zip':
					break;
				
				
				// Audio
				case 'audio/basic':
					break;
				
				case 'audio/voxware':
					break;
				
				case 'audio/x-aiff':
					break;
				
				case 'audio/x-midi':
					break;
				
				case 'audio/x-mpeg':
					break;
				
				case 'audio/x-wav':
					break;
				
				
				// Image
				case 'image/gif':
					break;
				
				case 'image/jpeg':
					$upload = $this->uploadFile($this->name, $this->type, $this->size, $this->media_type, $this->tmp_name, $this->error, '5242880', DIR_MEDIA_IMAGE, $this->tmp);
					return $upload;
					
					break;
				
				case 'image/png':
					$upload = $this->uploadFile($this->name, $this->type, $this->size, $this->media_type, $this->tmp_name, $this->error, '5242880', DIR_MEDIA_IMAGE, $this->tmp);
					return $upload;
					
					break;
				
				case 'image/tiff':
					break;
				
				case 'image/x-icon':
					break;
				
				
				// Text
				case 'text/css':
					break;
				
				case 'text/html':
					break;
				
				case 'text/javascript':
					break;
				
				case 'text/plain':
					break;
				
				case 'text/rtf':
					break;
				
				case 'text/xml':
					break;
				
				
				// Video
				case 'video/mpeg':
					break;
				
				case 'video/quicktime':
					break;
				
				case 'video/x-msvideo':
					break;
				
				
				// Default
				default:
					return $this->error(4);
					
					break;
			}
		} else return $this->error($this->error);
	}
	
	private function uploadFile($name, $type, $size, $media_type, $tmp_name, $error, $maxSize = '5242880', $uploadDir = DIR_MEDIA_TMP, $tmp = false) {
		// Examination of the various required fields
		$mids = array();
		
		$row = $this->eCMS->db->getArray("SELECT mid FROM {$this->eCMS->db_prefix}media");
		for($i = 0; $i < count($row); $i++) {
			$mids[] = $row[$i]['mid'];
		}
		
		// Generate new MID.
		if(count($mids >= 1)) {
			$finished = false;
			while($finished == false) {
				$mid = mt_rand(100000000, 999999999);
				
				if(in_array($mid, $mids)) $finished = false;
				else $finished = true;
			}
		}
		
		$ext = explode('.', $name);
		$ext = $ext[count($ext) - 1];
		if($tmp === true) $filename = DIR_MEDIA_TMP.$mid.'.'.$ext;
		else $filename = $uploadDir.$mid.'.'.$ext;
		
		if($this->size <= $maxSize) {
			if(is_uploaded_file($this->tmp_name)) {
				if(move_uploaded_file($this->tmp_name, $filename)) {
					// SET MEDIA
					$this->setMedia($mid, $mid.'.'.$ext, $type, $size, $media_type, true);
					return $this->error[0].$mid.'.'.$ext;
				} else return $this->error(7);
			} else return $this->error(10);
		} else return $this->error(2);
	}
	
	private function error($error) {
		$err = $this->errors[$error];
		
		return $err;
	}
	
	
	
	public function __destruct() {}
}
?>