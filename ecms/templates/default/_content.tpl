{extends file='./_layout.tpl'}

{block name=head}{include file='./_head.tpl'}{/block}

{block name=header}{include file='./_header.tpl'}{/block}

{block name=content_left}{include file='./_content_left.tpl'}{/block}
{block name=content_main}{include file='./_content_main.tpl'}{/block}
{block name=content_right}{include file='./_content_right.tpl'}{/block}

{block name=footer}{include file='./_footer.tpl'}{/block}