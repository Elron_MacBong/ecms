<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     function.icq.php
 * Type:     function
 * Name:     icq
 * Purpose:  display icq status online/offline/unknown
 * -------------------------------------------------------------
 */
function smarty_function_icq(array $params, Smarty_Internal_Template $template) {
	// Be sure icq parameter is present
	if(empty($params['icq'])) {  $smarty->trigger_error("icq: missing icq parameter"); return; }
	
	 $icq = $params['icq'];
	 $title = HTMLSpecialChars($params['title']);
	 
	 $output  = '';
	 $output .= '<span style="white-space:nowrap;">';
	 $output .= '  <a href="http://wwp.icq.com/scripts/contact.dll?msgto='.$icq.'">';
	 $output .= '    <img border="0" src="http://web.icq.com/scripts/online.dll?icq=' . $icq . '&amp;img=5" alt="'.$title.'" title="ICQ Messenger - '.$title.'" />';
	 $output .= '  </a>';
	 $output .= '</span>';
	 
	 return $output;
}
?>