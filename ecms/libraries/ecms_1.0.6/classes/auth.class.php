<?php
/*******************************************************\
| Functions:											|
| - void	public	__construct(object eCMS);			|
| - bool	public	checkPermission(string $pName);		|
| - bool	public	checkGroup(array $groups);			|
| - array	public	getGroups(string $uid = NULL);		|
| - array	public	getAllGroups();						|
| - void	public	__destruct();						|
\*******************************************************/

if(!defined('eCMS')) die('Hacking attempt...');

class Auth {
	private $eCMS;
	
	public function __construct($eCMS) {
		$this->eCMS = $eCMS;
	}
	
	
	
	// This function checks, if the user/guest has permissions-access.
	# @param string	$pName
	# 
	# @return bool
	public function checkPermission($pName) {
		if(!is_string($pName)) $this->eCMS->dieFunctionCall('checkPermission', 'pName', gettype($pName), 'string');
		
		// Read out the permission.
		$perm = $this->eCMS->db->getArray("SELECT pid, master, webmaster, admin, team, moderator, member, guest FROM {$this->eCMS->db_prefix}auth_permissions WHERE title = '".$this->eCMS->db->secureString('p_'.$pName)."'", MYSQL_ASSOC);
		// If permission exists...
		if(count($perm) === 1) {
			$perm = $perm[0];
			
			// ...we need many things to know.
			
			// First we needs to know, if the user is a guest.
			if($this->eCMS->security->checkLogin() === true) {
				// It's a user.
				// Now we needs to know, what groups our user have access.
				$uGroups = $this->eCMS->db->getArray("SELECT groups FROM {$this->eCMS->db_prefix}auth_permissions_user WHERE uid = '".$this->eCMS->db->secureString($_SESSION['eCMSuserUID'])."'");
				
				// We split the string into an array.
				$uGroups = explode(',', $uGroups[0]['groups']);
				
				// Read out the groups.
				$tmpGroups = $this->eCMS->db->getArray("SELECT gid, title, ual, master, webmaster, admin, team, moderator, member, guest FROM {$this->eCMS->db_prefix}auth_groups ORDER BY ual ASC");
				
				// Now we build the final array with the groups of our user.
				$groups = array();
				for($i = 0; $i < count($tmpGroups); $i++) {
					for($i2 = 0; $i2 < count($uGroups); $i2++) {
						if($uGroups[$i2] == $tmpGroups[$i]['gid']) $groups[] = $tmpGroups[$i];
					}
				}
				
				
				
				// Our group-array is now finished.
				// It is ordered from the lowest to the highest group and has the right override-rules basicly.
				// Now we start a new for() to override the access-variable many times.
				// With this step, we have the complete secure right checked and save&secure access to this permission!
				for($i = 0; $i < count($groups); $i++) {
					$row = $this->eCMS->db->getArray("SELECT status FROM {$this->eCMS->db_prefix}auth_permissions_group WHERE gid = '".$groups[$i]['gid']."' AND pid = '".$perm['pid']."'");
					
					// Override the access-variable and we are finish ;)
					// !!! But we need our override-rules for overriting the access-variable !!!
					// Remember
					// PERMISSION-SYSTEM
					//   |           OVERRIDES = 'XXXXX'           |
					//   |       NOT OVERRIDES = '     '           |
					//   | PERMISSION | DENIED | DEFAULT | ALLOWED | 
					//   |------------|--------|---------|---------|
					//   |   DENIED   | XXXXXX | XXXXXXX | XXXXXXX |
					//   |------------|--------|---------|---------|
					//   |   ALLOWED  |        | XXXXXXX | XXXXXXX |
					//   |------------|--------|---------|---------|
					//   |   DEFAULT  |        |         |         |
					//   |------------|--------|---------|---------|
					// 
					// But remember!
					// A higher group ALLOWED will be OVERRIDE a lower group DENIED !!!
					// Little bit tricky maybe?
					// Not realy, we haven't noticed the injection of the user-rules yet! ;)
					
					// We save the old access-state.
					if(isset($access) && $access !== '') $oldAccess = $access;
					else $oldAccess = 'DEFAULT';
					$newAccess = $row[0]['status'];
					
					/*
					echo 'GID: '.$groups[$i]['gid'].'<br />';
					echo 'OLD ACCESS: '.$oldAccess.'<br />';
					echo 'NEW ACCESS: '.$newAccess.'<br />';
					echo '<hr />';
					*/
					
					// Now we switch our new access.
					switch($newAccess) {
						// In the case, it will be override the complete state.
						// Needs2overwritten!
						case 'DENIED':
							$access = 'DENIED';
							
							break;
						
						// In this case, it will be override the state only, if the old access is NOT DENIED.
						case 'ALLOWED':
							/*
							if($oldAccess === 'DENIED')  $access = 'ALLOWED';
							if($oldAccess === 'DEFAULT') $access = 'ALLOWED';
							*/
							$access = 'ALLOWED';
							
							break;
						
						// This case is easy ;)
						// It gives the new $access the $oldAccess.
						case 'DEFAULT':
							$access = $oldAccess;
							
							break;
						
						// wtf in the fucking hell? There is really a fucking other state?
						// Ok, this is 100% a hack-try. But we are good and have seen the trap ;)
						// ACCESS DENIED!!! FUCKING HELL, YEAR!!! ;)
						default:
							$access = 'DENIED';
							
							break;
					}
				}
			} else {
				// It's a guest.
				// Now we handle ONLY guest-groups!
				// Guest-Group are putted into group 7 and it's undeleteable!
				
				// We read out, what the status of our permission for group 7 is.
				$status = $this->eCMS->db->getArray("SELECT status FROM {$this->eCMS->db_prefix}auth_permissions_group WHERE gid = '7' AND pid = '".$perm['pid']."'", MYSQL_ASSOC);
				$status = $status[0];
				$status = $status['status'];
				
				// Override the access-variable and we are finish ;)
				$access = $status;
			}
			
			// Now we check, what our access says.
			// Switch the access-variable and give 'true' or 'false'.
			switch($access) {
				case 'DEFAULT':
					return true;
					
					break;
				
				case 'ALLOWED':
					return true;
					
					break;
				
				case 'DENIED':
					return false;
					
					break;
				
				default:
					return false;
					
					break;
			}
		} else return false;
	}
	
	// This function checks, if the user/guest is in the given group-array and has access.
	# @param array	$groups
	# 
	# @return bool
	public function checkGroup($groups) {
		if(!is_array($groups)) $this->eCMS->dieFunctionCall('checkGroup', 'groups', gettype($groups), 'array');
		
		// We check, if our user is a guest...
		if($this->eCMS->security->checkLogin() === true) {
			// We have an user.
			// Now we have more then group 7.
			// The easiest way here is the in_array()-function.
			// But first, we need an array of the groups that our user has.
			// !!! NOTE: We haven't a check for for permissions at the moment. We needs to add this in future! !!!
			$uGroups = $this->eCMS->db->getArray("SELECT groups FROM {$this->eCMS->db_prefix}auth_permissions_user WHERE uid = '".$this->eCMS->db->secureString($_SESSION['eCMSuserUID'])."'");
			// We split the string into an array.
			$uGroups = explode(',', $uGroups[0]['groups']);
			
			// Now we have both arrays.
			// Let's work the in_array()-function.
			// But we needs a for()-loop for every group!
			$access = false;
			for($i = 0; $i < count($uGroups); $i++) {
				if(in_array($uGroups[$i], $groups)) $access = true;
			}
			
			return $access;
		} else {
			// We have a guest.
			// Than we needs to use only the group 7.
			if(in_array(7, $groups)) return true;
			else return false;
		}
	}
	
	// This function returns all available groups.
	# @param string	$uid	(default: NULL)
	# 
	# @return array
	public function getGroups($uid = NULL) {
		$ret = array();
		
		if($uid === NULL) {
			if($this->eCMS->security->checkLogin() === true) $ret = $this->eCMS->db->getArray("SELECT groups FROM {$this->eCMS->db_prefix}auth_permissions_user WHERE uid = '".$this->eCMS->db->secureString($_SESSION['eCMSuserUID'])."'");
		} else {
			if(!is_string($uid)) $this->eCMS->dieFunctionCall('getGroups', 'uid', gettype($uid), 'string');
			
			$ret = $this->eCMS->db->getArray("SELECT groups FROM {$this->eCMS->db_prefix}auth_permissions_user WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
		}
		
		return $ret;
	}
	
	// This function returns all groups.
	# @return array
	public function getAllGroups() {
		$ret = array();
		
		$row = $this->eCMS->db->getArray("SELECT gid, title FROM {$this->eCMS->db_prefix}auth_groups");
		
		if(count($row) > 0) $ret = $row;
		
		return $ret;
	}
	
	
	
	public function __destruct() {}
}
?>