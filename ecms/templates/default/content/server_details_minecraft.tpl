{* SMARTY *}
{* Used-Variables:
 * $variable - Description
 *}

{extends file='../_content.tpl'}

{block name=content}
{* Your code starts here! *}

<link href="{$templatePath}css/xxx.css" rel="stylesheet" type="text/css" />

<div>
	<div style="font-weight:bold; text-align:center;">Informationen</div>
	<div>
		<table width="100%">
			<tr>
				<td width="25%">Server-ID</td>
				<td width="75%">{$serverInfo.sid}</td>
			</tr>
			<tr>
				<td width="25%">Server-Host</td>
				<td width="75%">{$serverInfo.ip}:{$serverInfo.port}</td>
			</tr>
			<tr>
				<td width="25%">Server-Name</td>
				<td width="75%">{$serverInfo.name}</td>
			</tr>
		</table>
	</div>
</div>
<div>
	<div style="font-weight:bold; text-align:center;">Aktionen</div>
	<div style="text-align:center;">
		<span>Start</span> | 
		<span>Stop</span> | 
		<span>Restart</span>
	</div>
</div>

{* Your code ends here! *}
{/block}