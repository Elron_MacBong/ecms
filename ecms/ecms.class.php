<?php
/***************************************************************\
| Functions:													|
| - void	public	__construct(string param = NULL,			|
|								int mode = 0);					|
| - void	public	__destruct();								|
\***************************************************************/

// Hacking prevent.
if(!defined('eCMS')) die('Hacking attempt...');

// Start check.php.
require_once('./ecms/check.php');
define('DIR_CONFIG', str_replace('\\', '/', getcwd()).'/ecms/configs/');

require_once(DIR_CONFIG.'ecms.cfg.php');
require_once(DIR_CONFIG.'directory.cfg.php');
require_once(DIR_CONFIG.'database.cfg.php');
require_once(DIR_CONFIG.'smarty.cfg.php');

// Lets check for Smarty.
// We test later for smarty permissions.
if(!file_exists(DIR_LIB_SMARTY.'Smarty.class.php')) die("<span style='color:#FF0000; font-weight:bold;'>eCMS [CRITICAL]: 0x0000000022<br />Stopping loader to protect the system.<br /><br />Please contact the system administrator.<br /><br /><br /></span>");
require_once(DIR_LIB_SMARTY.'Smarty.class.php');
if(!class_exists('Smarty')) die("<span style='color:#FF0000; font-weight:bold;'>eCMS [CRITICAL]: 0x0000000023<br />Stopping loader to protect the system.<br /><br />Please contact the system administrator.<br /><br /><br /></span>");

class ecms extends Smarty {
	// Declare hardcoded constants.
	const eCMS_VERSION	= '1.0.5';
	
	// Dynamic variables.
	private $cmsMode;	// (0: normal, 1: API, 2: xajax)
	private $tpl;
	private $xajax;
	private $modeXAJAX = false;
	private $modeAPI = false;
	
	public $urlParams;
	public $db_settings;
	public $db_prefix;
	public $settings;
	public $module = array();
	public $handler_url;
	public $template;
	
	public $protection;
	public $log;
	public $db;
	public $security;
	public $auth;
	public $profile;
	public $user;
	public $upload;
	public $media;
	
	
	
	// The constructor.
	# @param string $type (default: NULL)
	# @param int    $mode (default: 0)
	# 
	# @return void
	public function __construct($param = NULL, $mode = 0) {
		if($param !== NULL && !is_string($param))	$this->dieFunctionCall('__construct (ecms.class.php)',	'param',	gettype($param),	'string');
		if($mode !== 0 && !is_int($mode))			$this->dieFunctionCall('__construct (ecms.class.php)',	'mode',		gettype($mode),		'integer');
		
		session_start();
		ob_start();
		
		// We bring in our protection-class.
		require_once(DIR_LIB_ECMS.'classes/protection.class.php');
		$this->protection = new Protection(clone $this);
		
		// Bring in the log-system.
		require_once(DIR_LIB_ECMS.'classes/log.class.php');
		$this->log = new Log(clone $this);
		
		// Now we need the database connection.
		require_once(DIR_LIB_ECMS.'classes/database/database_helper.class.php');
		global $MYSQL_SETTINGS;
		$this->db_settings = $MYSQL_SETTINGS;
		$this->db = new Database(clone $this, $this->db_settings, 'mysql');
		
		// Now we bring in the rest of the kernel-files.
		// Our protection-class is providing the existing and permissions all the time.
		if(!class_exists('Security'))	if(file_exists(DIR_LIB_ECMS.'classes/security.class.php'))	require_once(DIR_LIB_ECMS.'classes/security.class.php');
										else die("Can't include Security-System!");
		if(!class_exists('Auth'))		if(file_exists(DIR_LIB_ECMS.'classes/auth.class.php'))		require_once(DIR_LIB_ECMS.'classes/auth.class.php');
										else die("Can't include Auth-System!");
		if(!class_exists('User'))		if(file_exists(DIR_LIB_ECMS.'classes/user.class.php'))		require_once(DIR_LIB_ECMS.'classes/user.class.php');
										else die("Can't include User-System!");
		if(!class_exists('Profile'))	if(file_exists(DIR_LIB_ECMS.'classes/profile.class.php'))	require_once(DIR_LIB_ECMS.'classes/profile.class.php');
										else die("Can't include Profile-System!");
		if(!class_exists('Upload'))		if(file_exists(DIR_LIB_ECMS.'classes/upload.class.php'))	require_once(DIR_LIB_ECMS.'classes/upload.class.php');
										else die("Can't include Upload-System!");
		if(!class_exists('Media'))		if(file_exists(DIR_LIB_ECMS.'classes/media.class.php'))		require_once(DIR_LIB_ECMS.'classes/media.class.php');
										else die("Can't include Media-System!");
		
		
		
		// Set URL-Params.
		$this->initUrlParams($param);
		// Set mode
		$this->cmsMode = $mode;
		
		// Load the settings out of database.
		$this->loadSettings();
		
		// Template-Class construct.
		parent::__construct($this->settings['theme_default']);
		
		$this->initSmarty();
		
		// Now we need to add all of the available template-directories to Smarty.
		// We pick all templates out of database.
		$row = $this->db->getArray("SELECT theme_id, theme_dir FROM {$this->db_prefix}themes");
		
		// Generate arrays for $template_dir-variables.
		$tplArr	= array();
		$keys	= array();
		$values	= array();
		
		for($i = 0; $i < count($row); $i++) {
			array_push($keys, $row[$i]['theme_id']);
			array_push($values, DIR_TEMPLATE.$row[$i]['theme_dir']);
		}
		$tplArr = array_combine($keys, $values);
		
		// Give Smarty the array.
		$this->setTemplateDir($tplArr);
		
		
		
		// Now we bring in our static needed classes.
		// As first, we need our security-system.
		$this->security = new Security(clone $this);
		// As second comes our auth-system.
		$this->auth = new Auth(clone $this);
		
		// New we bring in the user and profile-systems.
		$this->user = new User(clone $this);
		$this->profile = new Profile(clone $this);
		
		// The upload and media-systems.
		$this->upload = new Upload(clone $this);
		$this->media = new Media(clone $this);
		
		
		
		// At least, we init the modules-array...
		$this->initModules();
		
		// and generate the navigation and widgets arrays.
		$this->assign('naviArr', $this->getNavi());
		//$this->assign('widgetsArr', $this->getWidgets());
		
		// Start template building
		if($mode !== 2) {
			if(count($this->urlParams) > 0 && $this->urlParams[0] === 'api') $this->initAPI();
			elseif($this->template['theme_xajax'] === 'TRUE') $this->initXajax();
			else $this->initNormal();
		}
	}
	
	
	
	// Setup the URL-Params-Array.
	# @param string
	# 
	# @return void
	private function initUrlParams($param) {
		if(!is_string($param)) $this->dieFunctionCall('initUrlParams', 'param', gettype($param), 'string');
		
		$paramArr = explode('/', $param);
		
		for($i = 0; $i < count($paramArr); $i++) {
			if($paramArr[$i] !== '') $this->urlParams[] = $paramArr[$i];
		}
	}
	
	// Load settings
	# @param int	$id (default: 1)
	# 
	# @return boolean
	private function loadSettings($id = 1) {
		if($id !== 1 && !is_int($param)) $this->dieFunctionCall('loadSettings', 'id', gettype($id), 'integer');
		
		$this->db_prefix = $this->db_settings[$id]['prefix'];
		$row = $this->db->getArray("SELECT variable, value FROM {$this->db_prefix}settings");
		
		for($i = 0; $i < count($row); $i++) {
			$settings[$row[$i]['variable']] = $row[$i]['value'];
		}
		
		if(!empty($settings)) {
			$this->settings = $settings;
			
			return true;
		} else return false;
	}
	
	// Init Smarty
	# @return boolean
	private function initSmarty() {
		// First init the complete Smarty things.
		// This is needed for perfect running of Smarty.
		
		// Set Smarty directories
		$this->setCacheDir(DIR_CACHE);
		$this->setCompileDir(DIR_COMPILE);
		$this->setConfigDir(DIR_CONFIG);
		// We set the template-directories later.
		
		// Now the other Smarty-settings.
		// Set PHP templates
		// !!! The PHP template file resource is an undocumented deprecated feature. !!!
		$this->allow_php_templates				= SMARTY_PHP_TEMPLATES;
		
		// Set auto literal
		$this->auto_literal						= SMARTY_AUTO_LITERAL;
		
		// Set caching
		$this->caching							= SMARTY_CACHING;
		// Set cache lifetime
		$this->cache_lifetime					= SMARTY_CACHE_LIFETIME;
		// Set cache looking
		#$this->cache_looking					= SMARTY_CACHE_LOOKING;
		// Set cache modified check
		$this->cache_modified_check				= SMARTY_CACHE_MODIFIED_CHECK;
		// Set caching type
		$this->caching_type						= SMARTY_CACHING_TYPE;
		
		// Set compile-id
		$this->compile_id						= SMARTY_COMPILE_ID;
		// Set compile looking
		#$this->compile_looking					= SMARTY_COMPILE_LOOKING;
		// Set compiler class
		#$this->compiler_class					= SMARTY_COMPILER_CLASS;
		
		// Set config booleanize
		$this->config_booleanize				= SMARTY_CONFIG_BOOLEANIZE;
		// Set config overwrite
		$this->config_overwrite					= SMARTY_CONFIG_OVERWRITE;
		// Set config read hidden
		$this->config_read_hidden				= SMARTY_CONFIG_READ_HIDDEN;
		// Set default config type
		$this->default_config_type				= SMARTY_DEFAULT_CONFIG_TYPE;
		
		// Set debugging
		$this->debugging						= SMARTY_DEBUGGING;
		// Set debug tpl
		#$this->debug_tpl						= SMARTY_DEBUG_TPL;
		// Set debugging ctrl
		$this->debugging_ctrl					= SMARTY_DEBUGGING_CTRL;
		
		// Set default modifiers
		#$this->default_modifiers				= SMARTY_DEFAULT_MODIFIERS;
		// Set default resource type
		$this->default_resource_type			= SMARTY_DEFAULT_RESOURCE_TYPE;
		// Set default template handler func
		$this->default_template_handler_func	= 'default_template_handler_func';
		
		// Set direct access security
		$this->direct_access_security			= SMARTY_DIRECT_ACCESS_SECURITY;
		
		
		
		// We read out the database informations about our picked template.
		$row = $this->db->getArray("SELECT theme_name, theme_dir, images_dir, theme_xajax FROM {$this->db_prefix}themes WHERE theme_id = '".$this->db->secureString($this->settings['theme_default'])."'", MYSQL_ASSOC);
		
		$this->template['theme_id']		= $this->settings['theme_default'];
		$this->template['theme_name']	= $row[0]['theme_name'].'/';
		$this->template['theme_dir']	= $row[0]['theme_dir'].'/';
		$this->template['images_dir']	= $row[0]['images_dir'].'/';
		$this->template['theme_xajax']	= $row[0]['theme_xajax'];
		
		$this->template['templateDir']	= 'ecms/templates/'.$row[0]['theme_dir'].'/';
		$this->template['imagesDir']	= 'ecms/templates/'.$row[0]['theme_dir'].'/'.$row[0]['images_dir'].'/';
		
		$this->template['templatePath']	= GENERAL_PAGE_URI.$this->template['templateDir'];
		$this->template['imagesPath']	= GENERAL_PAGE_URI.$this->template['imagesDir'];
		
		$this->template['pageTitle']	= $this->setPageTitle();
		
		// Assign variables
		$this->assign('pageTitle', 		$this->template['pageTitle']);
		$this->assign('pageUri',		GENERAL_PAGE_URI);
		$this->assign('theme_id',		$this->template['theme_dir']);
		$this->assign('theme_name',		$this->template['theme_name']);
		$this->assign('theme_dir',		$this->template['theme_dir']);
		$this->assign('images_dir',		$this->template['images_dir']);
		$this->assign('theme_xajax',	$this->template['theme_xajax']);
		
		$this->assign('templateDir',	$this->template['templateDir']);
		$this->assign('imagesDir',		$this->template['imagesDir']);
		
		$this->assign('templatePath',	$this->template['templatePath']);
		$this->assign('imagesPath',		$this->template['imagesPath']);
		
		$this->assign('jQueryDir',		DIR_LIB_JQUERY);
		$this->assign('jQueryPath',		GENERAL_PAGE_URI.'ecms/libraries/jquery_1.7.2/');
		
		/*
		$this->assign('jCropDir',		DIR_LIB_JCROP);
		$this->assign('jCropPath',		GENERAL_PAGE_URI.'ecms/libraries/jcrop_0.9.10/');
		
		$this->assign('jQueryUiDir',	DIR_LIB_JQUERY_UI);
		$this->assign('jQueryUiPath',	GENERAL_PAGE_URI.'ecms/libraries/jquery_ui_1.8.21/');
		*/
		
		$this->assign('modernizrDir',	DIR_LIB_MODERNIZR);
		$this->assign('modernizrPath',	GENERAL_PAGE_URI.'ecms/libraries/modernizr_2.5.3/');
		
		/*
		$this->assign('webforms2Dir',	DIR_LIB_WEBFORMS2);
		$this->assign('webforms2Path',	GENERAL_PAGE_URI.'ecms/libraries/webforms2_0.5.4/');
		*/
		
		$this->assign('xajaxDir',		DIR_LIB_XAJAX);
		$this->assign('xajaxPath',		GENERAL_PAGE_URI.'ecms/libraries/xajax_0.5/');
		
		if($this->cmsMode !== 2) {
			define('IMAGES_DIR', 	$this->template['imagesDir']);
			define('IMAGES_PATH',	$this->template['imagesPath']);
		}
	}
	
	// Load and init external modules.
	# @return void
	private function initModules() {
		$keys = array();
		$values = array();
		
		$row = $this->db->getArray("SELECT * FROM {$this->db_prefix}modules ORDER BY title ASC");
		
		for($i = 0; $i < count($row); $i++) {
			if(file_exists(DIR_LIB_ECMS.'modules/'.$row[$i]['file'])) require_once(DIR_LIB_ECMS.'modules/'.$row[$i]['file']);
			else die("Can't load module-class (".$row[$i]['title'].").<br />\nPlease contact the system administrator.");
			
			
			array_push($keys, $row[$i]['title']);
			array_push($values, new $row[$i]['class']($this));
		}
		
		$this->module = array_combine($keys, $values);
	}
	
	// Init normal.
	# @return void
	private function initNormal() {
		$this->modeXAJAX = false;
		$this->modeAPI = false;
		
		$this->buildTemplate();
	}
	// Init xajax.
	# @return void
	private function initXajax() {
		$this->modeXAJAX = true;
		$this->modeAPI = false;
		
		$this->buildTemplate();
	}
	// Init API.
	# @return void
	private function initAPI() {
		$this->modeXAJAX = false;
		$this->modeAPI = true;
		
		$this->buildTemplate();
	}
	
	
	
	// Set the right page-title.
	# @param string	$title	(default: NULL)
	# 
	# @return string
	public function setPageTitle($title = NULL) {
		$nTitle = '';
		
		if($title === NULL) $nTitle = GENERAL_PAGE_TITLE;
		else {
			if(!is_string($title)) $this->dieFunctionCall('setPageTitle', 'title', gettype($title), 'string');
			
			if($title === '') $nTitle = GENERAL_PAGE_TITLE;
			else $nTitle = $title.' - '.GENERAL_PAGE_TITLE;
		}
		
		return $nTitle;
	}
	
	// This function creates our navigation-array for the site-navigation.
	# @return array
	public function getNavi() {
		// Setup all variables.
		$navi		= array();
		$cats		= array();
		$entries	= array();
		$entries2	= array();
		
		// Read out the categories.
		$cats = $this->db->getArray("SELECT 
											cid, 
											title, 
											link, 
											pic 
										FROM 
											{$this->db_prefix}navi_categories 
										WHERE 
											hidden = 'FALSE' 
										ORDER BY 
											sort 
										ASC");
		
		// Start a for-loop and create the full array.
		for($i = 0; $i < count($cats); $i++) {
			// First, we check, if our user is logged in.
			if($this->security->checkLogin() === true) {
				// It's a user!
				// Now we needs to work with the full auth-system!
				
				// We read out the entries in an own array.
				$entries = $this->db->getArray("SELECT 
														eid, 
														title, 
														link, 
														sfile, 
														tplfile, 
														url, 
														extern, 
														groups 
													FROM 
														{$this->db_prefix}navi_entries 
													WHERE 
														cid = '".$this->db->secureString($cats[$i]['cid'])."' 
													AND 
														hidden = 'FALSE' 
													AND 
														show_guest_only = 'FALSE' 
													ORDER BY 
														sort 
													ASC");
			} else {
				// It's a guest!
				// Now we work only with guest-group!
				
				// We read out the entries in an own array.
				$entries = $this->db->getArray("SELECT 
														eid, 
														title, 
														link, 
														sfile, 
														tplfile, 
														url, 
														extern, 
														groups 
													FROM 
														{$this->db_prefix}navi_entries 
													WHERE 
														cid = '".$this->db->secureString($cats[$i]['cid'])."' 
													AND 
														hidden = 'FALSE' 
													ORDER BY 
														sort 
													ASC");
			}
			
			// We have all things, let's build the array.
			// Check for existing entries and push the navi-array.
			if(count($entries) > 0) {
				// Clear $entries2 to prevent errors.
				$entries2 = array();
				
				// Now we needs to check for the group 7 entry.
				for($i2 = 0; $i2 < count($entries); $i2++) {
					$groups = explode(',', $entries[$i2]['groups']);
					
					if($this->auth->checkGroup($groups) === true) $entries2[] = $entries[$i2];
					#$entries2[] = $entries[$i2];
				}
				
				if(count($entries2) > 0) 
					array_push($navi,	array(	'cid'		=> $cats[$i]['cid'], 
												'title'		=> $cats[$i]['title'], 
												'link'		=> $cats[$i]['link'], 
												'pic'		=> $cats[$i]['pic'], 
												'entries'	=> $entries2
											 )
							  );
			}
		}
		
		return $navi;
	}
	
	// Builds the template.
	# @param string $template (default: null)
	# @param string $cache_id (default: null)
	# @param string $compile_id (default: null)
	# 
	# @return void
	public function buildTemplate($template = null, $cache_id = null, $compile_id = null) {
		if($template !== null && !is_string($template))		$this->dieFunctionCall('buildTemplate', 'template',		gettype($template),		'string');
		if($cache_id !== null && !is_string($cache_id))		$this->dieFunctionCall('buildTemplate', 'cache_id',		gettype($cache_id),		'string');
		if($compile_id !== null && !is_string($template))	$this->dieFunctionCall('buildTemplate', 'compile_id',	gettype($compile_id),	'string');
		
		// First, we check, which mode is active.
		
		
		// We have API-Mode.
		if($this->modeAPI === true && $this->modeXAJAX === false) {
			die("<span style='color:#FF0000;'>eCMS [ERROR]: !!! API deactivated !!!</span>");
		}
		
		
		
		// We have xajax-Mode.
		if($this->modeAPI === false && $this->modeXAJAX === true) {
			// XAJAX Force Compile
			$this->force_compile = true;
			
			// ... load the lib ...
			require_once(DIR_LIB_XAJAX.'xajax_core/xajax.inc.php');
			
			// ... and bring in the xajax class.
			$this->xajax = new xajax();
			
			$this->xajax->configure('javascript URI', GENERAL_PAGE_URI.'eCMS/libraries/xajax_'.XAJAX_VERSION.'/');
			// XAJAX-Debug
			#$this->xajax->setFlags(array('debug' => true, 'outputEntities' => true));
			
			// Now we try to include  the xajax-module-file.
			#if(file_exists(DIR_XAJAX_CONTENT.$module.'.xajax.php')) require_once(DIR_XAJAX_CONTENT.$module.'.xajax.php');
			require_once(DIR_CONTENT_XAJAX.'general.xajax.php');
			
			// Get all xajax-module-files out of the database and try to load them.
			$this->loadXajaxModules();
			
			
			
			// Process the request and print out.
			$this->xajax->processRequest();
			$this->xajax->printJavascript();
			
			// Central display function, only for xajax-template.
			parent::display(DIR_TEMPLATE.$this->template['theme_dir'].'_layout.tpl');
		}
		
		
		
		// We have Normal-Mode.
		if($this->modeAPI === false && $this->modeXAJAX === false) {
			// Check for params.
			if(count($this->urlParams) > 0) {
				// Try to include give module or die.
				if(file_exists(DIR_CONTENT.'modules/'.$this->urlParams[0].'.module.php')) require_once(DIR_CONTENT.'modules/'.$this->urlParams[0].'.module.php');
				else die("Can't load module (".$this->urlParams[0].").<br />\nPlease contact the system administrator.");
			} else {
				// Try to load index module or die.
				if(file_exists(DIR_CONTENT.'modules/index.module.php')) require_once(DIR_CONTENT.'modules/index.module.php');
				else die("Can't load module (index).<br />\nPlease contact the system administrator.");
			}
		}
	}
	
	// Display out the template.
	// This function invokes and extends the smarty-display-function.
	# @param string	$template	(default: null)
	# @param string	$cache_id	(default: null)
	# @param string	$compile_id	(default: null)
	# 
	# @return void
	public function display($template = null, $cache_id = null, $compile_id = null) {
		if($template !== null && !is_string($template))		$this->dieFunctionCall('display', 'template',	gettype($template),		'string');
		if($cache_id !== null && !is_string($cache_id))		$this->dieFunctionCall('display', 'cache_id',	gettype($cache_id),		'string');
		if($compile_id !== null && !is_string($template))	$this->dieFunctionCall('display', 'compile_id',	gettype($compile_id),	'string');
		
		if($template === 'index') parent::display('file:['.$this->template['theme_id'].']content/'.$template.'.tpl', $cache_id, $compile_id);
		else {
			$tplArr = $this->getTemplateDir();
			if(file_exists($tplArr[$this->template['theme_id']].'content/'.$template.'.tpl')) parent::display('file:['.$this->template['theme_id'].']content/'.$template.'.tpl', $cache_id, $compile_id);
			else die("Can't load template (".$template.").<br />\nPlease contact the system administrator.");
		}
	}
	
	
	
	// Die the complete script with error-msg.
	// Needs to implement a log-function-call.
	# @param string	$function
	# @param string	$param
	# @param string $given
	# @param string $needed
	# 
	# @return void
	public function dieFunctionCall($function, $param, $given, $needed) {
		$error = false;
		
		if(!is_string($function)) {
			$this->dieFunctionCall('dieFunctionCall', 'function',	gettype($function),	'string');
			$error = true; 
		}
		if(!is_string($param)) {
			$this->dieFunctionCall('dieFunctionCall', 'param',		gettype($param),	'string');
			$error = true; 
		}
		if(!is_string($given)) {
			$this->dieFunctionCall('dieFunctionCall', 'given',		gettype($given),	'string');
			$error = true; 
		}
		if(!is_string($needed)) {
			$this->dieFunctionCall('dieFunctionCall', 'needed',		gettype($needed),	'string');
			$error = true; 
		}
		
		if($error === false) {
			// If our log-system is running, we give all infos first to the log-system.
			//$this->log->setLog('test', 'info');
			
			// Now we can die the whole script. R.I.P. ;)
			die(
				"
				<span style='color:#FF0000; font-weight:bold;'>eCMS [ERROR]: PARAMETER-TYPE WAS INCORECT!</span>
				<br />
				<br />
				<span style='color:#FF0000;'>Script aborted to protect the system.</span>
				<br />
				<span style='color:#FF0000;'>Informations logged.</span>
				<br />
				<br />
				<span style='color:#FF0000;'>Please contact the system-administrator.</span>
				"
				/*
				LOG-FILE
				<pre style='color:#FF0000;'>
 - Function  : ".$function."
 - Parameter : ".$param."
 - Given     : ".$given."
 - Needed    : ".$needed."
				</pre>
				*/
			);
		}
	}
	
	
	
	// The destructor.
	# @return void
	public function __destruct() {
		parent::__destruct();
		
		if($this->cmsMode !== 2 && ob_get_status()) ob_end_flush();
	}
}
?>