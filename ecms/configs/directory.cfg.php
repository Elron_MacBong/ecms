<?php
if(!defined('eCMS')) die('Hacking attempt...');

define('DIR_CACHE',				str_replace('\\', '/', getcwd()).'/ecms/cache/');
#define('DIR_CONFIG',			str_replace('\\', '/', getcwd()).'/ecms/configs/');
define('DIR_CONTENT',			str_replace('\\', '/', getcwd()).'/ecms/content/');
define('DIR_LIBS',				str_replace('\\', '/', getcwd()).'/ecms/libraries/');
define('DIR_LOGS',				str_replace('\\', '/', getcwd()).'/ecms/logs/');
define('DIR_MEDIA',				str_replace('\\', '/', getcwd()).'/ecms/media/');
define('DIR_TEMPLATE',			str_replace('\\', '/', getcwd()).'/ecms/templates/');
define('DIR_COMPILE',			str_replace('\\', '/', getcwd()).'/ecms/templates_c/');

define('DIR_CONTENT_API',		DIR_CONTENT.'api/');
define('DIR_CONTENT_MODULES',	DIR_CONTENT.'modules/');
define('DIR_CONTENT_WIDGETS',	DIR_CONTENT.'widgets/');
define('DIR_CONTENT_XAJAX',		DIR_CONTENT.'xajax/');

define('DIR_LIB_ECMS',			DIR_LIBS.'ecms_1.0.6/');
define('DIR_LIB_JQUERY',		DIR_LIBS.'jquery_1.7.2/');
define('DIR_LIB_MODERNIZR',		DIR_LIBS.'modernizr_2.5.3/');
define('DIR_LIB_SMARTY',		DIR_LIBS.'smarty_3.1.10/');
define('DIR_LIB_XAJAX',			DIR_LIBS.'xajax_0.5/');

define('DIR_MEDIA_TMP',			DIR_MEDIA.'tmp/');
define('DIR_MEDIA_APP',			DIR_MEDIA.'application/');
define('DIR_MEDIA_AUDIO',		DIR_MEDIA.'audio/');
define('DIR_MEDIA_IMAGE',		DIR_MEDIA.'image/');
define('DIR_MEDIA_TEXT',		DIR_MEDIA.'text/');
define('DIR_MEDIA_VIDEO',		DIR_MEDIA.'video/');
?>