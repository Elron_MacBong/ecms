<meta charset="utf-8" />

<title>{$pageTitle}</title>

<!-- Use the .htaccess and remove these lines to avoid edge case issues.
	  More info: h5bp.com/i/378 -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta name="description" content="" />

<!-- Mobile viewport optimized: h5bp.com/viewport -->
<meta name="viewport" content="width=device-width">

<!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->

<!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->