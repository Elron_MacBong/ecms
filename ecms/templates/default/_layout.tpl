<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<!-- Use the .htaccess and remove these lines to avoid edge case issues.
	  More info: h5bp.com/i/378 -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<title>{$pageTitle}</title>
	<meta name="description" content="">
	
	<!-- Mobile viewport optimized: h5bp.com/viewport -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->
	
	<!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
	
	
	
	<link href="{$templatePath}css/_html5.css" media="all" rel="stylesheet">
	<link href="{$templatePath}css/_layout_handheld.css" media="handheld" rel="stylesheet">
	<link href="{$templatePath}css/_layout_projection.css" media="projection" rel="stylesheet">
	<link href="{$templatePath}css/_layout_print.css" media="print" rel="stylesheet">
	<link href="{$templatePath}css/_layout_screen.css" media="screen" rel="stylesheet">
	<link href="{$templatePath}css/_layout_tv.css" media="tv" rel="stylesheet">
	<link href="{$templatePath}css/_html5_extra.css" media="all" rel="stylesheet">
	
	
	
	<script src="{$jQueryPath}jquery-1.7.2.min.js"></script>
	<script src="{$templatePath}js/_functions.js"></script>
</head>

</head>

<body>
<!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
	 chromium.org/developers/how-tos/chrome-frame-getting-started -->
<!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<div id="advt_overlay" class="advt_overlay" style="display:none;">DEFAULT_ADVT</div>

<div class="pagewidth" id="pagewidth">
	<a name="page_top" id="page_top"></a>
	<header class="header">{block name=header}DEFAULT_HEADER{/block}</header>
	
	<div id="wrapper" class="clearfix">
		<div class="content_left" id="content_left">{block name=content_left}DEFAULT_NAV{/block}</div>
		<div class="content_main" id="content_main">{block name=content_main}DEFAULT_CONTENT{/block}</div>
		<div class="content_right" id="content_right">{block name=content_right}DEFAULT_SIDEBAR{/block}</div>
	</div>
	<div class="clearfix"></div>
	
	<footer class="footer">{block name=footer}DEFAULT_FOOTER{/block}</footer>
</div>

<!-- JavaScript at the bottom for fast page loading -->
<!-- All JavaScript at the bottom, except this Modernizr build.
	 Modernizr enables HTML5 elements & feature detects for optimal performance.
	 Create your own custom Modernizr build: www.modernizr.com/download/ -->
<script src="{$modernizrPath}modernizr-2.5.3.min.js"></script>
<!-- <script src="{$webforms2Path}webforms2-p.js"></script> -->

<!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
	 mathiasbynens.be/notes/async-analytics-snippet -->
</body>
</html>