{* SMARTY *}
{* Used-Variables:
 * $variable - Description
 *}

{extends file='../_content.tpl'}

{block name=content}
{* Your code starts here! *}

<link href="{$templatePath}css/xxx.css" rel="stylesheet" type="text/css" />

<div style="text-align:center;">Willkommen auf der Server-&Uuml;bersicht</div>
<div style="text-align:center;"><a href="{$pageUri}server/new/">Neuen Server installieren</a></div>

<div>
	{if count($serverArr) > 0 }
		<table width="100%" border="0">
			<tr>
				<th width="20%" align="center">ID</th>
				<th width="50%" align="center">Name</th>
				<th width="30%" align="center">Host</th>
			</tr>
			{foreach name=serv_entry key=id item=serv_entry from=$serverArr}
				<tr>
					<td align="center"><a href="{$pageUri}server/{$serv_entry.sid}/">{$serv_entry.sid}</a></td>
					<td align="left"><a href="{$pageUri}server/{$serv_entry.sid}/">{$serv_entry.name}</a></td>
					<td align="right">{$serv_entry.ip}:{$serv_entry.port}</td>
				</tr>
			{/foreach}
		</table>
	{else}
		<div style="text-align:center" class="error">Keine Server installiert.</div>
	{/if}
</div>

{* Your code ends here! *}
{/block}