<?php
if(!defined('eCMS')) die('Hacking attempt...');

// NEEDED MODULE-VARIALES
$m_title		= 'Server';

// VARIABLES
$error			= false;
$errorString	= '';
$successString	= '';

$serverArr		= array();
$serverInfo		= array();

if($this->auth->checkPermission('server') === true) {
	if(count($this->urlParams) > 1) {
		if($this->urlParams[1] === 'new') {
			$m_title = $this->setPageTitle($m_title);
			
			$tpl = 'server_new';
		} elseif(is_numeric($this->urlParams[1])) {
			$ret = getServerInfo($this, $this->urlParams[1]);
			if($ret !== false) {
				$serverInfo = $ret;
				
				$m_title = $this->setPageTitle($ret['name']);
				
				$tpl = 'server_details_minecraft';
			} else {
				$serverArr = getServerList($this);
				
				$m_title = $this->setPageTitle($m_title.' - &Uuml;bersicht');
				
				$tpl = 'server_overview';
			}
		} else {
			$serverArr = getServerList($this);
			
			$m_title = $this->setPageTitle($m_title.' - &Uuml;bersicht');
			
			$tpl = 'server_overview';
		}
	} else {
		$serverArr = getServerList($this);
		
		$m_title = $this->setPageTitle($m_title.' - &Uuml;bersicht');
		
		$tpl = 'server_overview';
	}
} else {
	$m_title = $this->setPageTitle('Error');
	$errorString = 'Nicht genug Rechte oder nicht angemeldet!';
	
	$tpl = '_error';
}

function getServerList($eCMS) {
	$srvArr		= array();
	$srvArr2	= array();
	
	// First, read out the whole servers.
	$srvArr = $eCMS->db->getArray("SELECT
											sid,
											name,
											ip,
											port,
											users,
											groups
										FROM
											{$eCMS->db_prefix}servers
										WHERE
											enabled = 'TRUE'
										ORDER BY
											name
										ASC");
	
	// Now we will check, what servers are accessable for us.
	if(count($srvArr) > 0) {
		// Clear arrays to prevent errors.
		$userArr	= array();
		$groupArr	= array();
		
		// Start a loop for the $srvArr to check users and groups for every entry.
		for($i = 0; $i < count($srvArr); $i++) {
			$userArr	= explode(',', $srvArr[$i]['users']);
			$groupArr	= explode(',', $srvArr[$i]['groups']);
			
			if(count($userArr) > 0) {
				// Now check for every user, if it matches with the own uid.
				for($i2 = 0; $i2 < count($userArr); $i2++) {
					if($userArr[$i2] === $_SESSION['eCMSuserUID']) $srvArr2[] = $srvArr[$i];
				}
			}
		}
	}
	
	return $srvArr2;
}

function getServerInfo($eCMS, $sid) {
	$access = false;
	
	$ret = $eCMS->db->getArray("SELECT 
									sid,
									name,
									ip,
									port,
									enabled,
									users,
									groups
								FROM 
									{$eCMS->db_prefix}servers
								WHERE 
									sid = '$sid'");
	
	if(count($ret) === 1) {
		// Check for access.
		$userArr = explode(',', $ret[0]['users']);
		
		if(count($userArr) > 0) {
			for($i = 0; $i < count($userArr); $i++) {
				if($userArr[$i] === $_SESSION['eCMSuserUID']) $access = true;
			}
		}
		
		if($access === true) return $ret[0];
	} else return false;
}

$this->assign('pageTitle',		$m_title);
$this->assign('error',			$error);
$this->assign('errorString',	$errorString);
$this->assign('successString',	$successString);

$this->assign('serverArr',		$serverArr);
$this->assign('serverInfo',		$serverInfo);

$this->display($tpl);
?>